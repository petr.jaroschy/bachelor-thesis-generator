﻿using System;
using System.Collections.Generic;

namespace Bachelor_thesis_generator.Cards
{
	public static class ReferenceCardSets
	{
		public static List<IList<Card>> Decks = new List<IList<Card>>();
		static ReferenceCardSets()
		{
			#region CardTypes

			var singleTargetAttack1 = new Card
			{
				Cost = 4,
				CostType = (CostType)1,
				CardEffectType = (EffectType)1,
				BaseEffectValue = 6,
				BaseCardCost = 4,
				BaseEffectType = (EffectType)1,
				EffectValue = 6
			};

			var singleTargetAttack2 = new Card
			{
				Cost = 7,
				CostType = (CostType)1,
				CardEffectType = (EffectType)1,
				BaseEffectValue = 9,
				BaseCardCost = 7,
				BaseEffectType = (EffectType)1,
				EffectValue = 9
			};

			var singleTargetSpell1 = new Card
			{
				Cost = 2,
				CostType = 0,
				CardEffectType = (EffectType)1,
				BaseEffectValue = 5,
				BaseCardCost = 2,
				BaseEffectType = (EffectType)1,
				EffectValue = 5
			};

			var singleTargetSpell2 = new Card
			{
				Cost = 2,
				CostType = 0,
				CardEffectType = (EffectType)1,
				BaseEffectValue = 20,
				BaseCardCost = 8,
				BaseEffectType = (EffectType)1,
				EffectValue = 20
			};

			var aoeSpell1 = new Card
			{
				Cost = 4,
				CostType = 0,
				CardEffectType = (EffectType)1,
				BaseEffectValue = 4,
				BaseCardCost = 5,
				BaseEffectType = (EffectType)1,
				EffectValue = 4,
				CardType = CardType.NonTargetedEffect
			};

			var aoeSpell2 = new Card
			{
				Cost = 4,
				CostType = 0,
				CardEffectType = (EffectType)1,
				BaseEffectValue = 4,
				BaseCardCost = 5,
				BaseEffectType = (EffectType)1,
				EffectValue = 4,
				CardType = CardType.NonTargetedEffect
			};

			var aoeAttack1 = new Card
			{
				Cost = 6,
				CostType = (CostType)1,
				CardEffectType = (EffectType)1,
				BaseEffectValue = 2,
				BaseCardCost = 6,
				BaseEffectType = (EffectType)1,
				EffectValue = 2,
				CardType = CardType.NonTargetedEffect
			};

			var aoeAttack2 = new Card
			{
				Cost = 15,
				CostType = (CostType)1,
				CardEffectType = (EffectType)1,
				BaseEffectValue = 6,
				BaseCardCost = 15,
				BaseEffectType = (EffectType)1,
				EffectValue = 6,
				CardType = CardType.NonTargetedEffect
			};

			var differentCards = new List<Card>
			{
				aoeAttack2,
				aoeAttack1,
				singleTargetAttack2,
				singleTargetAttack1,
				singleTargetSpell1,
				singleTargetSpell2,
				aoeSpell1,
				aoeSpell2
			};

			#endregion

			#region SingleCardDecks

			var newDeck = new List<Card>();
			for (var i = 0; i < 15; i++)
			{
				newDeck.Add(aoeAttack1.Clone());
			}
			Decks.Add(newDeck);

			newDeck = new List<Card>();
			for (var i = 0; i < 15; i++)
			{
				newDeck.Add(aoeAttack2.Clone());
			}
			Decks.Add(newDeck);

			newDeck = new List<Card>();
			for (var i = 0; i < 15; i++)
			{
				newDeck.Add(aoeSpell1.Clone());
			}
			Decks.Add(newDeck);

			newDeck = new List<Card>();
			for (var i = 0; i < 15; i++)
			{
				newDeck.Add(aoeSpell1.Clone());
			}
			Decks.Add(newDeck);

			newDeck = new List<Card>();
			for (var i = 0; i < 15; i++)
			{
				newDeck.Add(singleTargetAttack1.Clone());
			}
			Decks.Add(newDeck);

			newDeck = new List<Card>();
			for (var i = 0; i < 15; i++)
			{
				newDeck.Add(singleTargetAttack2.Clone());
			}
			Decks.Add(newDeck);

			newDeck = new List<Card>();
			for (var i = 0; i < 15; i++)
			{
				newDeck.Add(singleTargetSpell1.Clone());
			}
			Decks.Add(newDeck);

			newDeck = new List<Card>();
			for (var i = 0; i < 15; i++)
			{
				newDeck.Add(singleTargetSpell2.Clone());
			}
			Decks.Add(newDeck);

			#endregion

			#region mixedDecks

			newDeck = new List<Card>();
			for (var i = 0; i < 5; i++)
			{
				newDeck.Add(singleTargetSpell2.Clone());
				newDeck.Add(singleTargetSpell1.Clone());
				newDeck.Add(aoeSpell1.Clone());
			}
			Decks.Add(newDeck);

			newDeck = new List<Card>();
			for (var i = 0; i < 5; i++)
			{
				newDeck.Add(singleTargetSpell1.Clone());
				newDeck.Add(singleTargetSpell2.Clone());
				newDeck.Add(aoeAttack2.Clone());
			}
			Decks.Add(newDeck);

			newDeck = new List<Card>();
			for (var i = 0; i < 5; i++)
			{
				newDeck.Add(singleTargetAttack2.Clone());
				newDeck.Add(singleTargetAttack1.Clone());
				newDeck.Add(aoeAttack1.Clone());
			}
			Decks.Add(newDeck);

			newDeck = new List<Card>();
			for (var i = 0; i < 5; i++)
			{
				newDeck.Add(singleTargetSpell1.Clone());
				newDeck.Add(singleTargetSpell2.Clone());
				newDeck.Add(aoeSpell1.Clone());
			}
			Decks.Add(newDeck);

			newDeck = new List<Card>();
			for (var i = 0; i < 5; i++)
			{
				newDeck.Add(singleTargetAttack2.Clone());
				newDeck.Add(singleTargetAttack1.Clone());
				newDeck.Add(aoeSpell2.Clone());
			}
			Decks.Add(newDeck);

			#endregion

			#region RandomDecks

			var rng = new Random(Seed: 142857);

			for (var j = 0; j < 20; j++)
			{
				newDeck = new List<Card>();
				for (var i = 0; i < 15; i++)
				{
					newDeck.Add(differentCards[rng.Next(minValue: 0, maxValue: 8)].Clone());
				}
				Decks.Add(newDeck);
			} 

			#endregion
		}
	}
}
