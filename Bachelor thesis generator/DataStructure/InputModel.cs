﻿using System.Collections.Generic;
using System.Linq;
using Bachelor_thesis_generator.Cards;

namespace Bachelor_thesis_generator.DataStructure
{
	public class InputModel
	{
		public Dictionary<int, List<Enemy>> PowerMarks;

		public int Generations;
		public int NodeCount;
		public int GenerationSize;

		public double StrengthenOpProb;
		public double WeakenOpProb;
		public double MergeOpProb;
		public double SplitOpProb;
		public double AddNodeOpProb;

		public double WinRateWeight;
		public double AverageWinTurnWeight;
		public double MedianWinTurnWeight;
		public double BalanceWeight;
		public double MfcWeight;
		public double GraciousLossWeight;

		public InputModel(Dictionary<int, List<Enemy>> powerMarks, int generations, int nodeCount,
			double strengthenOpProb, double weakenOpProb, double mergeOpProb, double splitOpProb, double addNodeOpProb,
			int generationSize, double winRateWeight = 1d,
			double averageWinTurnWeight = 1d, double medianWinTurnWeight = 1d, double balanceWeight = 1d,
			double mfcWeight = 1d, double graciousLossWeight = 1d)
		{
			PowerMarks = powerMarks;
			Generations = generations;
			NodeCount = nodeCount;
			StrengthenOpProb = strengthenOpProb;
			WeakenOpProb = weakenOpProb;
			MergeOpProb = mergeOpProb;
			SplitOpProb = splitOpProb;
			AddNodeOpProb = addNodeOpProb;
			WinRateWeight = winRateWeight;
			AverageWinTurnWeight = averageWinTurnWeight;
			MedianWinTurnWeight = medianWinTurnWeight;
			BalanceWeight = balanceWeight;
			MfcWeight = mfcWeight;
			GraciousLossWeight = graciousLossWeight;
			GenerationSize = generationSize;
		}

		public InputModel Clone()
		{
			var dictionary = new Dictionary<int, List<Enemy>>();

			foreach (var powerMarksKey in PowerMarks.Keys)
			{
				dictionary.Add(powerMarksKey, PowerMarks[powerMarksKey].Select(x => x.Clone()).ToList());
			}

			return new InputModel(dictionary, Generations, NodeCount, StrengthenOpProb, WeakenOpProb, MergeOpProb,
				SplitOpProb, AddNodeOpProb, GenerationSize, WinRateWeight, AverageWinTurnWeight, MedianWinTurnWeight, BalanceWeight,
				MfcWeight, GraciousLossWeight);
		}
	}
}
