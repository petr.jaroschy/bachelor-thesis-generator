﻿using System.Collections.Generic;
using System.Linq;

namespace Bachelor_thesis_generator.DataStructure
{
	public class NodeTree
	{
		public List<Node> NodeList;

		public NodeTree(Node root)
		{
			Root = root;
			NodeList = new List<Node> {root};
		}

		public Node Root { get; }

		public static NodeTree FromEntities(IEnumerable<NodeEntity> entities)
		{
			var enumerable = entities.ToList();
			var nodeEntities = enumerable.ToDictionary(x => x.Id);
			var nodes = enumerable.Select(Node.FromEntity).ToDictionary(x => x.Id);

			foreach (var node in nodes.Values)
			{
				var children = nodeEntities[node.Id].ChildrenIds.Select(id => nodes[id]).ToList();

				node.Children = children;
			}

			return new NodeTree(nodes[key: 1])
			{
				NodeList = nodes.Values.ToList()
			};
		}

		public void InitTree()
		{
			NodeList = new List<Node>();
			var x = 1;
			InitTree(Root, ref x);
		}

		private void InitTree(Node node, ref int curId, Node parent = null)
		{
			node.Id = curId;
			node.Parent = parent;
			NodeList.Add(node);
			foreach (var nodeChild in node.Children)
			{
				++curId;
				InitTree(nodeChild, ref curId, node);
			}
		}

		public NodeTree Clone()
		{
			var nodesInList = new List<Node>();
			return new NodeTree(CloneNode(Root, parent: null, nodeList: nodesInList))
			{
				NodeList = nodesInList
			};
		}

		private static Node CloneNode(Node node, Node parent, ICollection<Node> nodeList)
		{
			var nodeToReturn = node.Clone();
			nodeList.Add(nodeToReturn);
			nodeToReturn.Parent = parent;
			nodeToReturn.Children = node.Children.Select(x => CloneNode(x, nodeToReturn, nodeList)).ToList();
			return nodeToReturn;
		}
	}
}