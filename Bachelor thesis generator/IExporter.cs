﻿using Bachelor_thesis_generator.DataStructure;

namespace Bachelor_thesis_generator
{
	public interface IExporter
	{
		string Export(NodeTree tree);
		// ReSharper disable once UnusedMember.Global
		NodeTree Import(string serialized);
	}
}