﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Bachelor_thesis_generator
{
	internal class NonDominatedSorter
	{
		/// <summary>
		/// Returns list of ranked trees, sorted from best to worst
		/// </summary>
		/// <param name="ratedTrees">Rated trees to sort</param>
		/// <returns>non dominated sorted list of ranked trees</returns>
		internal static List<RankedRatedTree> NonDominatedSort(List<Generator.RatedTree> ratedTrees)
		{
			var list = ratedTrees.Select(x => new RankedRatedTree
			{
				Tree = x.Tree,
				MostFrequentCaseRating = x.MostFrequentCaseRating,
				MedianWinTurnRating = x.MedianWinTurnRating,
				AverageWinTurnRating = x.AverageWinTurnRating,
				WinRateRating = x.WinRateRating,
				BalanceRating = x.BalanceRating,
				GraciousLossRating = x.GraciousLossRating
			}).ToList();

			foreach (var rankedRatedTree in list)
			{
				foreach (var rankedRatedTree2 in list)
				{
					if (Dominates(rankedRatedTree,rankedRatedTree2))
					{
						rankedRatedTree.DominatesTrees.Add(rankedRatedTree2);
					}
					if (Dominates(rankedRatedTree2, rankedRatedTree))
					{
						rankedRatedTree2.DominatesTrees.Add(rankedRatedTree);
					}
				}
			}

			var front = list;
			var nextFront = new List<RankedRatedTree>();
			var currentRank = 1;

			while (front.Count > 0)
			{
				foreach (var rankedRatedTree in front)
				{
					rankedRatedTree.DominatesTrees.ForEach(x =>
					{
						x.Rank = currentRank + 1;
						nextFront.Add(x);
					});
				}

				front = nextFront;
				currentRank++;
				nextFront = new List<RankedRatedTree>();
			}

			GetCrowdingDistanceFor(x => x.AverageWinTurnRating, list);
			GetCrowdingDistanceFor(x => x.BalanceRating, list);
			GetCrowdingDistanceFor(x => x.MedianWinTurnRating, list);
			GetCrowdingDistanceFor(x => x.MostFrequentCaseRating, list);
			GetCrowdingDistanceFor(x => x.WinRateRating, list);
			GetCrowdingDistanceFor(x => x.GraciousLossRating, list);

			list.Sort((tree, ratedTree) => tree.Rank < ratedTree.Rank ? -1 : tree.Rank > ratedTree.Rank ? 1 : tree.CrowdingDistance > ratedTree.CrowdingDistance ? -1 : 1);

			return list;
		}

		/// <summary>
		/// Computes crowding distances for entities for specified property.
		/// Call this for all properties of the entity in any order
		/// </summary>
		/// <param name="func">getter of property of our entity</param>
		/// <param name="list">list of our entities</param>
		private static void GetCrowdingDistanceFor(Func<RankedRatedTree, double> func,
			List<RankedRatedTree> list)
		{
			// sort the entities by one of their properties
			list.Sort((tree,
					ratedTree) => func(tree) < func(ratedTree)
					? 1 : func(tree) > func(ratedTree)
						? -1 : 0);

			//set crowding distances to infinity for the edge values
			list[index: 0].CrowdingDistance = double.PositiveInfinity;
			list[list.Count - 1].CrowdingDistance = double.PositiveInfinity;

			//increase crowding distances depending on the value proximity
			for (var i = 1; i < list.Count - 1; i++)
			{
				list[i].CrowdingDistance += func.Invoke(list[i + 1]) - func.Invoke(list[i - 1]);
			}
		}

		private static bool Dominates(Generator.RatedTree first, Generator.RatedTree second)
		{
			return first.AverageWinTurnRating > second.AverageWinTurnRating &&
			       first.BalanceRating > second.BalanceRating &&
			       first.MedianWinTurnRating > second.MedianWinTurnRating &&
			       first.MostFrequentCaseRating > second.MostFrequentCaseRating &&
			       first.WinRateRating > second.WinRateRating &&
			       first.GraciousLossRating > second.GraciousLossRating;
		}

		internal class RankedRatedTree : Generator.RatedTree
		{
			public List<RankedRatedTree> DominatesTrees = new List<RankedRatedTree>();
			public int Rank = 1;
			public double CrowdingDistance;
		}
	}
}
