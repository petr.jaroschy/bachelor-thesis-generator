﻿using System;
using System.Linq;
using Bachelor_thesis_generator.Cards;
using Bachelor_thesis_generator.DataStructure;

namespace Bachelor_thesis_generator.Operators
{
	internal class SplitNodeOp : IOperator
	{
		private readonly Random random;
		private readonly double prob;

		public SplitNodeOp(Random random, double prob)
		{
			this.random = random;
			this.prob = prob;
		}

		public NodeTree Mutate(NodeTree tree, InputModel input)
		{
			if (random.NextDouble() > prob || tree.Root.Children.Count == 0)
			{
				return null;
			}

			var withoutRoot = tree.NodeList.Where(x => x.Id != 1).ToList();
			var totalStrength = withoutRoot.Select(x => x.NodePower).Sum();

			var randomNum = random.Next(totalStrength);
			var i = -1;
			do
			{
				i++;
				randomNum -= withoutRoot[i].NodePower;
			} while (randomNum > 0);

			var chosen = withoutRoot[i];
			var children = chosen.Children;
			var ratio = random.NextDouble();

			var left = new Node
			{
				Depth = chosen.Depth,
				NodePower = (int) Math.Round(chosen.NodePower * ratio),
				NodeState = NodeState.None,
				NodeTargets = chosen.NodeTargets.ToArray()
			};
			var right = new Node
			{
				Depth = chosen.Depth,
				NodePower = (int) Math.Round(chosen.NodePower * (1 - ratio)),
				NodeState = NodeState.None,
				NodeTargets = chosen.NodeTargets.ToArray()
			};

			foreach (var child in children)
			{
				if (random.Next(maxValue: 2) == 1)
				{
					left.Children.Add(child);
				}
				else
				{
					right.Children.Add(child);
				}
			}

			chosen.Parent.Children.Add(left);
			chosen.Parent.Children.Add(right);
			chosen.Parent.Children.Remove(chosen);

			tree.InitTree();
			return tree;
		}
	}
}
