﻿using System;
using System.Linq;
using Bachelor_thesis_generator.DataStructure;

namespace Bachelor_thesis_generator.Operators
{
	internal class StrengthenOp : IOperator
	{
		private readonly Random random;
		private readonly double prob;

		public StrengthenOp(Random random, double prob)
		{
			this.random = random;
			this.prob = prob;
		}

		public NodeTree Mutate(NodeTree tree, InputModel input)
		{
			if (random.NextDouble() > prob || tree.Root.Children.Count == 0)
			{
				return null;
			}

			var withoutRoot = tree.NodeList.Where(x => x.Id != 1).ToList();
			var maxStrength = withoutRoot.Select(x => x.NodePower).Max() + 1;
			var invertedStrengths = withoutRoot.Select(x => maxStrength - x.NodePower).ToList();
			var invertedTotalStrength = invertedStrengths.Sum();

			var randomNum = random.Next(invertedTotalStrength);
			var i = -1;
			do
			{
				i++;
				randomNum -= invertedStrengths[i];
			} while (randomNum > 0);

			var chosen = withoutRoot[i];

			chosen.NodePower += random.Next(chosen.Depth + 1);

			tree.InitTree();
			return tree;
		}
	}
}
