﻿using System;
using System.Linq;
using Bachelor_thesis_generator.DataStructure;

namespace Bachelor_thesis_generator.Operators
{
	internal class WeakenOp : IOperator
	{
		private readonly Random random;
		private readonly double prob;

		public WeakenOp(Random random, double prob)
		{
			this.random = random;
			this.prob = prob;
		}

		public NodeTree Mutate(NodeTree tree, InputModel input)
		{
			if (random.NextDouble() > prob || tree.Root.Children.Count == 0)
			{
				return null;
			}

			var withoutRoot = tree.NodeList.Where(x => x.Id != 1).ToList();
			var totalStrength = withoutRoot.Select(x => x.NodePower).Sum();

			var randomNum = random.Next(totalStrength);
			var i = -1;
			do
			{
				i++;
				randomNum -= withoutRoot[i].NodePower;
			} while (randomNum > 0);

			var chosen = withoutRoot[i];

			chosen.NodePower -= random.Next(maxValue: 3);
			chosen.NodePower = chosen.NodePower < 0 ? 0 : chosen.NodePower;

			tree.InitTree();
			return tree;
		}
	}
}
