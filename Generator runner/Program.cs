﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Bachelor_thesis_generator;
using Bachelor_thesis_generator.Cards;
using Bachelor_thesis_generator.DataStructure;
using Newtonsoft.Json;

namespace Generator_runner
{
	internal class Program
	{
		private const string ResourcesFile = "../../../Resources/";

		private static void Main(string[] args)
		{
			var loadEncounters = LoadEncounters(args.Select(x => ResourcesFile + x + ".json").ToArray());

			var inputModel = new InputModel(loadEncounters.ToDictionary(x => loadEncounters.IndexOf(x)),
				generations: 5000,
				nodeCount: 26,
				generationSize: 30,
				strengthenOpProb: 0.8,
				weakenOpProb: 0.8,
				mergeOpProb: 0.5,
				splitOpProb: 0.5,
				addNodeOpProb: 0.5,
				winRateWeight: 1d,
				averageWinTurnWeight: 0.3,
				mfcWeight: 1d,
				medianWinTurnWeight: 0.7,
				balanceWeight: 1d,
				graciousLossWeight: 0.8);

			var tree = Generator.GenerateEvo(inputModel);

			IExporter exporter = new JsonExporter();

			var outStr = exporter.Export(tree);
			File.WriteAllText(path: "../../../Out/WeightedEvo.json", contents: outStr);
		}

		public static List<List<Enemy>> LoadEncounters(string[] encountersPath)
		{
			var encounters = new List<List<Enemy>>();
			foreach (var path in encountersPath)
			{
				var newList = new List<Enemy>();
				var reader = File.OpenText(path);
				var jsonStrings = reader.ReadToEnd();
				var enemySetEntity = JsonConvert.DeserializeObject<EnemySetEntity>(jsonStrings);
				newList.AddRange(enemySetEntity.Entities.Select(FromEntity));
				encounters.Add(newList);
			}

			return encounters;
		}

		private static Enemy FromEntity(EnemyEntity entity)
		{
			var enemy = new Enemy
			{
				Health = entity.Defense,
				Attack = entity.Attack,
				BaseAttack = entity.Attack,
				BaseHealth = entity.Defense,
				ThreatValue = (int)(20 * (entity.Attack / Math.Sqrt(entity.Defense)))
			};
			return enemy;
		}
	}
}