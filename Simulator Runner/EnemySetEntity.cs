﻿using System;

namespace Simulator_Runner
{
	[Serializable]
	internal class EnemySetEntity
	{
		public EnemyEntity[] Entities;
	}

	[Serializable]
	internal class EnemyEntity
	{
		public int Attack;
		public int Defense;
	}
}