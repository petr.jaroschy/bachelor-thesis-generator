﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Bachelor_thesis_generator.Cards;
using Bachelor_thesis_generator.DataStructure;
using Newtonsoft.Json;
using Simulator;

namespace Simulator_Runner
{
	class Program
	{
		private const string ResourcesFile = "../../../Resources/";

		static void Main(string[] args)
		{
			var reader = File.OpenText(ResourcesFile + args[0] + ".json");
			var jsonStrings = reader.ReadToEnd();
			var nodeEntities = JsonConvert.DeserializeObject<NodeEntities>(jsonStrings);
			var tree = NodeTree.FromEntities(nodeEntities.Entities);
			tree.InitTree();
			var loadEncounters = LoadEncounters(args.Skip(1).Select(x => ResourcesFile + x + ".json").ToArray());

			var simulator = new Simulator.Simulator();

			var result = simulator.Simulate(Cards.cards, tree, loadEncounters, "HillClimbing");

			new CsvExport().Export(result, "../../../out/HillClimbing.csv");
		}

		public static List<List<Enemy>> LoadEncounters(string[] encountersPath)
		{
			var encounters = new List<List<Enemy>>();
			foreach (var path in encountersPath)
			{
				var newList = new List<Enemy>();
				var reader = File.OpenText(path);
				var jsonStrings = reader.ReadToEnd();
				var enemySetEntity = JsonConvert.DeserializeObject<EnemySetEntity>(jsonStrings);
				newList.AddRange(enemySetEntity.Entities.Select(FromEntity));
				encounters.Add(newList);
			}

			return encounters;
		}

		private static Enemy FromEntity(EnemyEntity entity)
		{
			var enemy = new Enemy
			{
				Health = entity.Defense,
				Attack = entity.Attack,
				BaseAttack = entity.Attack,
				BaseHealth = entity.Defense,
				ThreatValue = (int)(20 * (entity.Attack / Math.Sqrt(entity.Defense)))
			};
			return enemy;
		}
	}
}
