﻿using System.Collections.Generic;
using System.Linq;
using Bachelor_thesis_generator.Cards;
using MoreLinq;
using Simulator.Common;
using Simulator.DataStructures;

namespace Simulator.Bots
{
	class GreedyBot : Bot
	{
		protected override int AiSeed => 0;
		public override EncounterResult FightEncounter(Game game)
		{
			while (true) //turns
			{
				var state = game.IsOver();

				if (state != null)
				{
					if (state == true) throw new WtfException();
					return GenerateResult(game);
				}

				while (true) //actions
				{
					var cards = GetPlayableCards(game);

					if (cards.Count == 0)
					{
						game.EndTurn();
						break;
					}

					cards = RankCards(cards, game);
					var chosenCard = cards[index: 0];
					var enemies = game.Enemies.Where(x => x.Health > 0).ToList();
					var target = chosenCard.CardType == CardType.TargetedEffect
						? enemies.Where(x => x.Health < chosenCard.EffectValue)
							.MaxBy(x => x.Attack).FirstOrDefault() ?? enemies.MaxBy(x => x.Attack/x.Health).First()
						: null;

					game.PlayCard(chosenCard, target);

					state = game.IsOver();

					if (state == null) continue;
					if (state == false) throw new WtfException();
					return GenerateResult(game);
				}
			}
		}

		public GreedyBot(List<int> nodeIds) : base(nodeIds)
		{
		}
	}
}
