﻿using System.Collections.Generic;
using System.Linq;
using Bachelor_thesis_generator.Cards;
using Simulator.Common;
using Simulator.DataStructures;

namespace Simulator.Bots
{
	class LuckyBot :Bot
	{
		protected override int AiSeed => 0;
		public override EncounterResult FightEncounter(Game game)
		{
			while (true)
			{
				var luckyTurnSimulator = new LuckyTurnSimulator(new TurnSimulator.TurnState()
				{
					Health = game.Health,
					Enemies = game.Enemies.Select(x => x.Clone()).ToList(),
					Energy = game.Energy,
					Hand = game.Hand.ToList(),
					Mana = game.Mana,
					MaxEnergy = 20,
					MaxHealth = 50,
					MaxMana = 10
				}, game.Deck.Take(5).ToList());
				luckyTurnSimulator.Simulate();
				var bestState = luckyTurnSimulator.BestTurnState;

				var reversePlayList = new List<Card>();
				var reverseEnemyList = new List<Enemy>();

				while (bestState != null)
				{
					if (bestState.played != null)
					{
						reversePlayList.Add(bestState.played);
						reverseEnemyList.Add(game.Enemies.FirstOrDefault(x => x.Guid == bestState.target?.Guid));
					}

					bestState = bestState.Parent;
				}

				bool? state;
				for (int i = reversePlayList.Count - 1; i >= 0; i--)
				{
					game.PlayCard(reversePlayList[i], reverseEnemyList[i]);
					state = game.IsOver();
					if (state == false) throw new WtfException();
					if (state == true) return GenerateResult(game);
				}

				game.EndTurn();

				state = game.IsOver();
				if (state == true) throw new WtfException();
				if (state == false) return GenerateResult(game);
			}
		}

		public LuckyBot(List<int> nodeIds) : base(nodeIds)
		{
		}
	}
}
